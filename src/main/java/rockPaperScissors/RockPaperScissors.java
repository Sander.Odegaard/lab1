package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    int tmp = 0;
    String computerChoice = "";
    String humanChoice = "";
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        
        play();
        computerHand();
        humanHand();
        winner();
        if(tmp == 1){
            System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Human wins!");
        }
        if(tmp == 2){
            System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". Computer wins!");
        }
        if(tmp == 3){
            System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". It's a tie!");
        }
        score();
        playAgain();

    }

    public void play(){
        System.out.println("Let's play round "+ roundCounter);
    }

    public void computerHand(){
        String [] choices = {"rock", "paper", "scissors" };
        Double randNum = Math.random()*3;
        int choice = randNum.intValue();
        computerChoice = choices[choice];
    }

    public void humanHand() {
        String answer = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
        if (rpsChoices.contains(answer)){
            humanChoice = answer;
        }
        else {
            System.out.println("I do not understand " + answer +". Could you try again?");
            humanHand();
        }
    }

    public void playAgain() {
        String yesOrNo = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
        if (yesOrNo.equals("y")){
            roundCounter ++;
            run();
        }
        else if (yesOrNo.equals("n")){
            System.out.println("Bye bye :)");
        }
        else {
            System.out.println("I don't understand "+yesOrNo+". Try again");
            playAgain();
        }
    }

    public void winner(){
        if ((humanChoice.equals("rock") && computerChoice.equals("scissors")) || (humanChoice.equals("paper") && computerChoice.equals("rock")) || (humanChoice.equals("scissors") && computerChoice.equals("paper"))){
            humanScore ++;
            tmp = 1;
        }
        else if((computerChoice.equals("rock") && humanChoice.equals("scissors")) || (computerChoice.equals("paper") && humanChoice.equals("rock")) || (computerChoice.equals("scissors") && humanChoice.equals("paper"))){
            computerScore ++;
            tmp = 2;
        }
        else {
            tmp = 3;
        }
    }

    public void score(){
        System.out.println("Score: human "+humanScore+", computer "+computerScore);
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
